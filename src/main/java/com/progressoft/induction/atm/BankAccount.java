package com.progressoft.induction.atm;

import java.util.ArrayList;
import java.util.List;

public class BankAccount {

 private    String accountNo;
   private Double balance;

    public BankAccount(String accountNo, Double balance) {
        this.accountNo = accountNo;
        this.balance = balance;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }


    public static List<BankAccount> getBankAccounts() {

        List<BankAccount> bankAccounts = new ArrayList<>();

        BankAccount bankAccount1 = new BankAccount("123456789", 1000.0);
        BankAccount bankAccount2 = new BankAccount("111111111", 1000.0);
        BankAccount bankAccount3 = new BankAccount("222222222", 1000.0);
        BankAccount bankAccount4 = new BankAccount("333333333", 1000.0);
        BankAccount bankAccount5 = new BankAccount("444444444", 1000.0);

        bankAccounts.add(bankAccount1);
        bankAccounts.add(bankAccount2);
        bankAccounts.add(bankAccount3);
        bankAccounts.add(bankAccount4);
        bankAccounts.add(bankAccount5);

        return bankAccounts;
    }

}
