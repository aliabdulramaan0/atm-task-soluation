package com.progressoft.induction.atm;

import java.util.ArrayList;
import java.util.List;

public class BankNoteValue {

 private    Banknote banknote;
   private Integer number;
Double amount;
    public BankNoteValue(Banknote banknote, Integer number) {
        this.banknote = banknote;
        this.number = number;
        this.amount=number*banknote.getValue().doubleValue();
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setBanknote(Banknote banknote) {
        this.banknote = banknote;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }




    public static List<BankNoteValue> initilizeAtm() {
        List<BankNoteValue> bankNoteValues = new ArrayList<>();
        BankNoteValue fifty = new BankNoteValue(Banknote.FIFTY_JOD, 10);
        BankNoteValue twenty = new BankNoteValue(Banknote.TWENTY_JOD, 20);
        BankNoteValue ten = new BankNoteValue(Banknote.TEN_JOD, 100);
        BankNoteValue five = new BankNoteValue(Banknote.FIVE_JOD, 100);

        bankNoteValues.add(fifty);
        bankNoteValues.add(twenty);
        bankNoteValues.add(ten);
        bankNoteValues.add(five);

        return bankNoteValues;
    }

}
