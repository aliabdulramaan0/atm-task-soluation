package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ATMMachine implements ATM, BankingSystem {

    List<BankAccount> bankAccounts = BankAccount.getBankAccounts();
    List<BankNoteValue> bankNoteValues = BankNoteValue.initilizeAtm();
    Double atmBalance = getATMBalance();

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {

        BankAccount bankAccount = getBankAccountByAccountNo(accountNumber);

        if (Objects.isNull(bankAccount)) throw new AccountNotFoundException();

        if (bankAccount.getBalance() < amount.doubleValue()) throw new InsufficientFundsException();

        if (amount.doubleValue() > atmBalance) throw new NotEnoughMoneyInATMException();


        List<Banknote> banknotes = getBankNotes(accountNumber, amount);

        return banknotes;
    }


    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        BankAccount bankAccount = getBankAccountByAccountNo(accountNumber);
        if (Objects.nonNull(bankAccount.getBalance())) {
            return new BigDecimal(bankAccount.getBalance());
        } else return null;
    }


    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {

        BankAccount bankAccount = getBankAccountByAccountNo(accountNumber);
        if (Objects.nonNull(bankAccount) && bankAccount.getBalance() >= amount.doubleValue())
            bankAccount.setBalance(bankAccount.getBalance() - amount.doubleValue());


    }


    private List<Banknote> getBankNotes(String accountNumber, BigDecimal amount) {


        debitAccount(accountNumber, amount);

        return updateATMBalance(amount.doubleValue());
    }


    public BankAccount getBankAccountByAccountNo(String accountNumber) {

        BankAccount bankAccount = bankAccounts.stream().filter(b -> b.getAccountNo().equals(accountNumber)).findFirst().orElse(null);

        return bankAccount;

    }

    private double getATMBalance() {

        double amount = bankNoteValues.stream().mapToDouble(BankNoteValue::getAmount).sum();
        return amount;
    }


    private List<Banknote> updateATMBalance(double amount) {

        atmBalance -= amount;

        List<Banknote>banknotes=new ArrayList<>();

        while (amount!=0){
            List<Integer> counters = new ArrayList<>();
        for (BankNoteValue bankNoteValue : bankNoteValues) {
            Integer counter = 0;
            if (amount >= bankNoteValue.getBanknote().getValue().doubleValue()) {

                if (bankNoteValue.getNumber() <= (amount / (int) bankNoteValue.getBanknote().getValue().doubleValue())) {
                    counter = bankNoteValue.getNumber();
                } else {
                    counter = (int) amount / ((int) bankNoteValue.getBanknote().getValue().doubleValue());
                }

                if (counter!=1&&amount % bankNoteValue.getBanknote().getValue().doubleValue() == 0.0 &&!bankNoteValue.getBanknote().getValue().equals(new BigDecimal("5.0"))) {

               counter = counter - 1;
                }

                amount = BigDecimal.valueOf(amount - bankNoteValue.getBanknote().getValue().doubleValue() * counter).doubleValue();



            }
            counters.add(counter);
            bankNoteValue.setNumber(bankNoteValue.getNumber() - counter);

        }

        banknotes.addAll(getBankNote(counters));
        }

        return banknotes;
    }

    private List<Banknote> getBankNote(List<Integer> cs) {
        List<Banknote> banknotes = new ArrayList<>();
        for (int i = 0; i < cs.get(0); i++) {
            banknotes.add(Banknote.FIFTY_JOD);
        }
        for (int i = 0; i < cs.get(1); i++) {
            banknotes.add(Banknote.TWENTY_JOD);
        }

        for (int i = 0; i < cs.get(2); i++) {
            banknotes.add(Banknote.TEN_JOD);
        }
        for (int i = 0; i < cs.get(3); i++) {
            banknotes.add(Banknote.FIVE_JOD);
        }
        return banknotes;
    }

}


